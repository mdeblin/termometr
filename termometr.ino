
#include <string.h>
#include <avr/pgmspace.h>
#include <Wire.h> 

uint8_t font_5x7w[95][5] = {
  {0x00,0x00,0x00,0x00,0x00}, //
  {0x00,0x00,0xfa,0x00,0x00}, // !
  {0x00,0xe0,0x00,0xe0,0x00}, // "
  {0x28,0xfe,0x28,0xfe,0x28}, // #
  {0x24,0x54,0xfe,0x54,0x48}, // $
  {0xc4,0xc8,0x10,0x26,0x46}, // %
  {0x6c,0x92,0xaa,0x44,0x0a}, // &
  {0x00,0xa0,0xc0,0x00,0x00}, // '
  {0x00,0x38,0x44,0x82,0x00}, // (
  {0x00,0x82,0x44,0x38,0x00}, // )
  {0x10,0x54,0x38,0x54,0x10}, // *
  {0x10,0x10,0x7c,0x10,0x10}, // +
  {0x00,0x0a,0x0c,0x00,0x00}, // ,
  {0x10,0x10,0x10,0x10,0x10}, // -
  {0x00,0x06,0x06,0x00,0x00}, // .
  {0x04,0x08,0x10,0x20,0x40}, // /
  {0x7c,0x8a,0x92,0xa2,0x7c}, // 0
  {0x00,0x42,0xfe,0x02,0x00}, // 1
  {0x42,0x86,0x8a,0x92,0x62}, // 2
  {0x84,0x82,0xa2,0xd2,0x8c}, // 3
  {0x18,0x28,0x48,0xfe,0x08}, // 4
  {0xe4,0xa2,0xa2,0xa2,0x9c}, // 5
  {0x3c,0x52,0x92,0x92,0x0c}, // 6
  {0x80,0x8e,0x90,0xa0,0xc0}, // 7
  {0x6c,0x92,0x92,0x92,0x6c}, // 8
  {0x60,0x92,0x92,0x94,0x78}, // 9
  {0x00,0x6c,0x6c,0x00,0x00}, // :
  {0x00,0x6a,0x6c,0x00,0x00}, // ;
  {0x00,0x10,0x28,0x44,0x82}, // <
  {0x28,0x28,0x28,0x28,0x28}, // =
  {0x82,0x44,0x28,0x10,0x00}, // >
  {0x40,0x80,0x8a,0x90,0x60}, // ?
  {0x4c,0x92,0x9e,0x82,0x7c}, // @
  {0x7e,0x88,0x88,0x88,0x7e}, // A
  {0xfe,0x92,0x92,0x92,0x6c}, // B
  {0x7c,0x82,0x82,0x82,0x44}, // C
  {0xfe,0x82,0x82,0x44,0x38}, // D
  {0xfe,0x92,0x92,0x92,0x82}, // E
  {0xfe,0x90,0x90,0x80,0x80}, // F
  {0x7c,0x82,0x82,0x8a,0x4c}, // G
  {0xfe,0x10,0x10,0x10,0xfe}, // H
  {0x00,0x82,0xfe,0x82,0x00}, // I
  {0x04,0x02,0x82,0xfc,0x80}, // J
  {0xfe,0x10,0x28,0x44,0x82}, // K
  {0xfe,0x02,0x02,0x02,0x02}, // L
  {0xfe,0x40,0x20,0x40,0xfe}, // M
  {0xfe,0x20,0x10,0x08,0xfe}, // N
  {0x7c,0x82,0x82,0x82,0x7c}, // O
  {0xfe,0x90,0x90,0x90,0x60}, // P
  {0x7c,0x82,0x8a,0x84,0x7a}, // Q
  {0xfe,0x90,0x98,0x94,0x62}, // R
  {0x62,0x92,0x92,0x92,0x8c}, // S
  {0x80,0x80,0xfe,0x80,0x80}, // T
  {0xfc,0x02,0x02,0x02,0xfc}, // U
  {0xf8,0x04,0x02,0x04,0xf8}, // V
  {0xfe,0x04,0x18,0x04,0xfe}, // W
  {0xc6,0x28,0x10,0x28,0xc6}, // X
  {0xc0,0x20,0x1e,0x20,0xc0}, // Y
  {0x86,0x8a,0x92,0xa2,0xc2}, // Z
  {0x00,0x00,0xfe,0x82,0x82}, // [
  {0x40,0x20,0x10,0x08,0x04}, // "\"
  {0x82,0x82,0xfe,0x00,0x00}, // ]
  {0x20,0x40,0x80,0x40,0x20}, // ^
  {0x02,0x02,0x02,0x02,0x02}, // _
  {0x00,0x80,0x40,0x20,0x00}, // `
  {0x04,0x2a,0x2a,0x2a,0x1e}, // a
  {0xfe,0x12,0x22,0x22,0x1c}, // b
  {0x1c,0x22,0x22,0x22,0x04}, // c
  {0x1c,0x22,0x22,0x12,0xfe}, // d
  {0x1c,0x2a,0x2a,0x2a,0x18}, // e
  {0x10,0x7e,0x90,0x80,0x40}, // f
  {0x10,0x28,0x2a,0x2a,0x3c}, // g
  {0xfe,0x10,0x20,0x20,0x1e}, // h
  {0x00,0x22,0xbe,0x02,0x00}, // i
  {0x04,0x02,0x22,0xbc,0x00}, // j
  {0x00,0xfe,0x08,0x14,0x22}, // k
  {0x00,0x82,0xfe,0x02,0x00}, // l
  {0x3e,0x20,0x18,0x20,0x1e}, // m
  {0x3e,0x10,0x20,0x20,0x1e}, // n
  {0x1c,0x22,0x22,0x22,0x1c}, // o
  {0x3e,0x28,0x28,0x28,0x10}, // p
  {0x10,0x28,0x28,0x18,0x3e}, // q
  {0x3e,0x10,0x20,0x20,0x10}, // r
  {0x12,0x2a,0x2a,0x2a,0x04}, // s
  {0x20,0xfc,0x22,0x02,0x04}, // t
  {0x3c,0x02,0x02,0x04,0x3e}, // u
  {0x38,0x04,0x02,0x04,0x38}, // v
  {0x3c,0x02,0x0c,0x02,0x3c}, // w
  {0x22,0x14,0x08,0x14,0x22}, // x
  {0x30,0x0a,0x0a,0x0a,0x3c}, // y
  {0x22,0x26,0x2a,0x32,0x22}, // z
  {0x00,0x10,0x6c,0x82,0x00}, // {
  {0x00,0x00,0xfe,0x00,0x00}, // |
  {0x00,0x82,0x6c,0x10,0x00}, // }
  {0x40,0x80,0xc0,0x40,0x80}, // ~
};


// TMP75 Address is 0x48 and from its Datasheet = A0,A1,A2 are all grounded.
int TMP75_Address = 0x48; 

#define PINS 8
int PIN[PINS] = {
  42,40,38,36,34,32,30,28
};

#define VNEG  3
#define RST   20
#define CS1   26
#define CS2   24
#define CS3   22 
#define DI    44
#define RW    46
#define E     48



#define DISPLAY_ON   0b00111001
#define DISPLAY_OFF  0b00111000

#define PAGE0  0b00001110
#define PAGE1  0b01001110
#define PAGE2  0b10001110
#define PAGE3  0b11001110

#define UP     0b00111011
#define DOWN   0b00111010

int decPlaces = 1;
int numOfBytes = 2;
int baudRate = 9600;
int columns = 16;   // LCD is 16 Column 2 Row Hitachi LCD
int rows = 2;
byte configReg = 0x01;  // Address of Configuration Register
byte bitConv = B01100000;   // Set to 12 bit conversion
byte rdWr = 0x01;       // Set to read write
byte rdOnly = 0x00;    // Set to Read
String temperature, SerialDegC, lcdDegC;

int val,in;                           


void setup(){
  Serial.begin(baudRate);
  tone(VNEG,2000);
  pinMode(RW,OUTPUT);
  pinMode(E,OUTPUT);
  pinMode(DI,OUTPUT);
  pinMode(CS1,OUTPUT);
  pinMode(CS2,OUTPUT);
  pinMode(CS3,OUTPUT);
  pinMode(RST,OUTPUT);


  for (int i=0; i < PINS ; ++i) {
    pinMode(PIN[i],OUTPUT);
  }


  digitalWrite(CS1,HIGH);
  digitalWrite(CS2,HIGH);
  digitalWrite(CS3,HIGH);
  digitalWrite(RST,LOW);
  delay(50);
  digitalWrite(RST,HIGH);

  Wire.begin();                      // Join the I2C bus as a master
  lcdDegC += "'C";
  Wire.beginTransmission(TMP75_Address);       // Address the TMP75 sensor
  Wire.write(configReg);                       // Address the Configuration register 
  Wire.write(bitConv);                         // Set the temperature resolution 
  Wire.endTransmission();                      // Stop transmitting
  Wire.beginTransmission(TMP75_Address);       // Address the TMP75 sensor
  Wire.write(rdOnly);                          // Address the Temperature register 
  Wire.endTransmission();        

  lcd_set_address(PAGE0);

  lcd_set_address(DISPLAY_OFF); 
  clrscr();
  lcd_set_address(DISPLAY_ON); 

    print_string(0,"Arduino Mega 2560");
    print_string(30,"-------------------");
    print_string(66,"Temperature");
   }

void loop(){
  float temp = readTemp();             // Read the temperature now
   print_string(99,(String) temp + lcdDegC); 
  print_string(28,"*");
  delay(500);                          // Slow the reads down so we can follow the output
  print_string(28," ");
  delay(500);
}

// Begin the reading the TMP75 Sensor 
float readTemp(){
  // Now take a Temerature Reading
  Wire.requestFrom(TMP75_Address,numOfBytes);  // Address the TMP75 and set number of bytes to receive
  byte MostSigByte = Wire.read();              // Read the first byte this is the MSB
  byte LeastSigByte = Wire.read();             // Now Read the second byte this is the LSB

  // Being a 12 bit integer use 2's compliment for negative temperature values
  int TempSum = (((MostSigByte << 8) | LeastSigByte) >> 4); 
  // From Datasheet the TMP75 has a quantisation value of 0.0625 degreesC per bit
  float temp = (TempSum*0.0625);
  //  Serial.println(MostSigByte, BIN);   // Uncomment for debug of binary data from Sensor
  //  Serial.println(LeastSigByte, BIN);  // Uncomment for debug  of Binary data from Sensor
  return temp;                           // Return the temperature value
}

byte invert_bits(byte n) {
 byte bit=0;
 for (int i=0; i<8; i++) {
   bitWrite(bit,7-i,bitRead(n,i));
 }
 return bit;
}

void lcd_setpixel(int X, int Y, byte MODE){
  int cs,row,rpos;
  cs=0;
  row=0;
  rpos=0;

  digitalWrite(CS1,LOW);
  digitalWrite(CS2,LOW);
  digitalWrite(CS3,LOW);

  cs=abs(X/50);

  switch (cs) {
    case 1:
    digitalWrite(CS2,HIGH);
    break;
    case 2:
    digitalWrite(CS3,HIGH);
    break;
    default: 
    digitalWrite(CS1,HIGH);
  }

  row=X/150;
  rpos=X%150;

  switch (row) {
    case 1:
    rpos=(rpos%50)+64;
    break;
    case 2:
    rpos=(rpos%50)+128;
    break;
    case 3:
    rpos=(rpos%50)+192;
    break;
    default:
    rpos=rpos%50;
  }

  lcd_set_address(rpos);
  //lcd_putdata (VAL);

  digitalWrite(CS1,HIGH);
  digitalWrite(CS2,HIGH);
  digitalWrite(CS3,HIGH);
}

byte lcd_read_status () {
  byte value=0;
  //Set port in to the INPUT mode
  for (int i=0; i < PINS ; ++i) {
    pinMode(PIN[i],INPUT);
  }

  digitalWrite(DI,LOW);
  digitalWrite(RW,HIGH); 
  digitalWrite(E,HIGH); 
  delay(1);
  for (int i=0; i < PINS ; ++i) {
    bitWrite(value,i,digitalRead(PIN[i]));
  }
  delay(1);
  digitalWrite(E,LOW); 
  digitalWrite(RW,LOW);   
  //Set port in to the OUTPUT mode
  for (int i=0; i < PINS ; ++i) {
    pinMode(PIN[i],OUTPUT);
  }
  return value;
}

void lcd_set_data (byte num) {
  for (int i=0; i < PINS ; ++i) {
    digitalWrite(PIN[i],bitRead(num,i));
  }
}

void lcd_set_address (int addr) {
  digitalWrite(DI,LOW);
  digitalWrite(RW,LOW);
  digitalWrite(E,HIGH);
  lcd_set_data (addr);
  digitalWrite(E,LOW);
  digitalWrite(RW,HIGH);
}

void lcd_putdata (int data) {
  digitalWrite(RW,LOW);
  digitalWrite(DI,HIGH);
  digitalWrite(E,HIGH);
  lcd_set_data(data);
  digitalWrite(E,LOW);
  digitalWrite(DI,LOW);
  digitalWrite(RW,HIGH);
}

void lcd_setpoint(int POS, byte VAL){
  int cs,row,rpos;
  cs=0;
  row=0;
  rpos=0;

  digitalWrite(CS1,LOW);
  digitalWrite(CS2,LOW);
  digitalWrite(CS3,LOW);

  cs=abs(POS/50);

  switch (cs) {
    case 1:
    digitalWrite(CS2,HIGH);
    break;
    case 2:
    digitalWrite(CS3,HIGH);
    break;
    case 3:
    digitalWrite(CS1,HIGH);
    break;
    case 4:
    digitalWrite(CS2,HIGH);
    break;
    case 5:
    digitalWrite(CS3,HIGH);
    break;
    case 6:
    digitalWrite(CS1,HIGH);
    break;
    case 7:
    digitalWrite(CS2,HIGH);
    break;
    case 8:
    digitalWrite(CS3,HIGH);
    break;
    case 9:
    digitalWrite(CS1,HIGH);
    break;
    case 10:
    digitalWrite(CS2,HIGH);
    break;
    case 11:
    digitalWrite(CS3,HIGH);
    break;
    default: 
    digitalWrite(CS1,HIGH);
  }
  row=POS/150;
  rpos=POS%150;

  switch (row) {
    case 1:
    rpos=(rpos%50)+64;
    break;
    case 2:
    rpos=(rpos%50)+128;
    break;
    case 3:
    rpos=(rpos%50)+192;
    break;
    default:
    rpos=rpos%50;
  }

  lcd_set_address(rpos);
  lcd_putdata (VAL);

  digitalWrite(CS1,HIGH);
  digitalWrite(CS2,HIGH);
  digitalWrite(CS3,HIGH);
}

void clrscr(){
  for (int a=0; a<50; a++){
    lcd_set_address (a);
    lcd_putdata (0);
  }
  for (int a=64; a<114; a++){
    lcd_set_address (a);
    lcd_putdata (0);
  }
  for (int a=128; a<178; a++){
    lcd_set_address (a);
    lcd_putdata (0);
  }
  for (int a=192; a<242; a++){
    lcd_set_address (a);
    lcd_putdata (0);
  }
}

void print_char (int rpos, char Char) {
  for (int count=0; count < 5; count++){
    lcd_setpoint(rpos,invert_bits(font_5x7w[int(Char)-32][count]));
    rpos++;
  }
}


void print_string(int pos, String str) {
  pos*=5;
  for (int i=0; i < str.length(); i++){
    print_char(pos,str.charAt(i));
    pos=pos+8;
  }
}
